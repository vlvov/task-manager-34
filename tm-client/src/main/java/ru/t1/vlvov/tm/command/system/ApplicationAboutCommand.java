package ru.t1.vlvov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.Request.ProjectClearRequest;
import ru.t1.vlvov.tm.dto.Request.ServerAboutRequest;
import ru.t1.vlvov.tm.dto.Response.ServerAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String DESCRIPTION = "Show developer info.";

    @NotNull
    private final String NAME = "about";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @Nullable final ServerAboutRequest request = new ServerAboutRequest();
        @NotNull ServerAboutResponse response = getSystemEndpoint().getAbout(request);
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

}
