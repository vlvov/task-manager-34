package ru.t1.vlvov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.UserProfileRequest;
import ru.t1.vlvov.tm.dto.Response.UserProfileResponse;
import ru.t1.vlvov.tm.enumerated.Role;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-view-profile";

    @NotNull
    private final String DESCRIPTION = "Display user profile.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER VIEW PROFILE]");
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @NotNull final UserProfileResponse response = getUserEndpoint().profile(request);
        showUser(response.getUser());
    }

}