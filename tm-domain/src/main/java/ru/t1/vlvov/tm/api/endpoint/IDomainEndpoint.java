package ru.t1.vlvov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.*;
import ru.t1.vlvov.tm.dto.Response.AbstractResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndpoint{

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    AbstractResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64LoadRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64SaveRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinaryLoadRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinarySaveRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadJaxBRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveJaxBRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse loadDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadJaxBRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse saveDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveJaxBRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlSaveFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    AbstractResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveFasterXmlRequest request
    );

}
