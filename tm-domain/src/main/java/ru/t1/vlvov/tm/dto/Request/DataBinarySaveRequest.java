package ru.t1.vlvov.tm.dto.Request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class DataBinarySaveRequest extends AbstractUserRequest {

    public DataBinarySaveRequest(@Nullable String token) {
        super(token);
    }

}
