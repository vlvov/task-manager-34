package ru.t1.vlvov.tm.dto.Request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class DataXmlLoadJaxBRequest extends AbstractUserRequest {

    public DataXmlLoadJaxBRequest(@Nullable String token) {
        super(token);
    }

}
