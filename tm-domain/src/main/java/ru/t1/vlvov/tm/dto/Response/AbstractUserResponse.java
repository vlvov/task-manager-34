package ru.t1.vlvov.tm.dto.Response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.vlvov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    private User user;

    public AbstractUserResponse(User user) {
        this.user = user;
    }
}
