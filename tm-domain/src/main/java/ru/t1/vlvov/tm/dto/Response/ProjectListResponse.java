package ru.t1.vlvov.tm.dto.Response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.vlvov.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListResponse extends AbstractResponse {

    private List<Project> projects;

    public ProjectListResponse(List<Project> projects) {
        this.projects = projects;
    }

}
