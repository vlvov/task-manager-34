package ru.t1.vlvov.tm.dto.Response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.Project;

@NoArgsConstructor
public final class ProjectUpdateByIndexResponse extends AbstractProjectResponse {

    public ProjectUpdateByIndexResponse(@Nullable Project project) {
        super(project);
    }

}
