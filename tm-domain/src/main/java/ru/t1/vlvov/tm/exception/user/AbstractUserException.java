package ru.t1.vlvov.tm.exception.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.exception.AbstractException;

public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException() {
        super();
    }

    public AbstractUserException(@Nullable final String message) {
        super(message);
    }

    public AbstractUserException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(@Nullable final Throwable cause) {
        super(cause);
    }

    protected AbstractUserException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            @Nullable final boolean enableSuppression,
            @Nullable final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
